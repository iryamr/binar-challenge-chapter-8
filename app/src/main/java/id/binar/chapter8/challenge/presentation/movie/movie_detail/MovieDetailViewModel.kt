package id.binar.chapter8.challenge.presentation.movie.movie_detail

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter8.challenge.domain.model.movie.Movie
import id.binar.chapter8.challenge.domain.repository.MovieRepository
import id.binar.chapter8.challenge.util.Result
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val repository: MovieRepository
) : ViewModel() {

    var state by mutableStateOf(MovieDetailState())

    private val resultChannel = Channel<Result<Movie>>()

    init {
        getMovieDetail()
    }

    private fun getMovieDetail() {
        viewModelScope.launch {
            val movieId = savedStateHandle.get<Int>("movieId") ?: return@launch
            state = state.copy(isLoading = true)
            val result = repository.getMovieDetail(movieId)
            state = state.copy(
                isLoading = false,
                movie = result.data
            )
            resultChannel.send(result)
        }
    }
}