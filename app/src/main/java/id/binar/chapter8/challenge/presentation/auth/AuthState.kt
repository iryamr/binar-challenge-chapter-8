package id.binar.chapter8.challenge.presentation.auth

import id.binar.chapter8.challenge.domain.model.auth.User

data class AuthState(
    val isLoading: Boolean = false,

    val signUpEmail: String = "",
    val signUpUsername: String = "",
    val signUpPassword: String = "",
    val signUpRepeatedPassword: String = "",

    val signInEmail: String = "",
    val signInUsername: String = "",
    val signInPassword: String = "",

    val emailError: String? = null,
    val usernameError: String? = null,
    val passwordError: String? = null,
    val repeatedPasswordError: String? = null,

    val username: String = "",
    val user: User? = null
)
