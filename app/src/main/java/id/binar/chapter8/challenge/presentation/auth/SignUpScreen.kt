package id.binar.chapter8.challenge.presentation.auth

import android.widget.Toast
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import id.binar.chapter8.challenge.presentation.destinations.MovieScreenDestination
import id.binar.chapter8.challenge.presentation.destinations.SignUpScreenDestination
import id.binar.chapter8.challenge.ui.theme.Blue
import id.binar.chapter8.challenge.ui.theme.LightBlue
import id.binar.chapter8.challenge.util.AuthResult
import id.binar.chapter8.challenge.util.Constants

@ExperimentalMaterialApi
@ExperimentalFoundationApi
@Composable
@Destination
fun SignUpScreen(
    navigator: DestinationsNavigator,
    viewModel: AuthViewModel = hiltViewModel()
) {
    val state = viewModel.state
    val context = LocalContext.current
    LaunchedEffect(viewModel, context) {
        viewModel.authResult.collect { result ->
            when (result) {
                is AuthResult.Authorized -> {
                    navigator.navigate(MovieScreenDestination) {
                        popUpTo(SignUpScreenDestination.route) {
                            inclusive = true
                        }
                    }
                }
                is AuthResult.Unauthorized -> {
                }
                is AuthResult.UnknownError -> {
                    Toast.makeText(
                        context,
                        "An unknown error occurred",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(32.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Hello!",
            fontSize = 40.sp,
            modifier = Modifier
                .fillMaxWidth(),
            color = Blue
        )
        Text(
            text = "Sign up to find your favorite movies!",
            fontSize = 16.sp,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 4.dp),
            color = Blue,
        )

        Spacer(modifier = Modifier.height(24.dp))
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            colors = TextFieldDefaults.outlinedTextFieldColors(
                cursorColor = Blue,
                disabledLabelColor = LightBlue,
                focusedLabelColor = Blue,
                focusedBorderColor = Blue,
                unfocusedBorderColor = LightBlue,
            ),
            label = {
                Text(text = "Username")
            },
            shape = RoundedCornerShape(12.dp),
            singleLine = true,
            value = state.signUpUsername,
            onValueChange = {
                if (it.length <= Constants.MAX_LENGTH_USERNAME) {
                    viewModel.onEvent(AuthUiEvent.SignUpUsernameChanged(it))
                }
            },
            isError = state.usernameError != null,
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
        )
        if (state.usernameError != null) {
            Text(
                text = state.usernameError,
                fontSize = 12.sp,
                color = MaterialTheme.colors.error,
                modifier = Modifier
                    .padding(top = 4.dp)
                    .align(Alignment.Start)
            )
        }

        Spacer(modifier = Modifier.height(16.dp))
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            colors = TextFieldDefaults.outlinedTextFieldColors(
                cursorColor = Blue,
                disabledLabelColor = LightBlue,
                focusedLabelColor = Blue,
                focusedBorderColor = Blue,
                unfocusedBorderColor = LightBlue,

                ),
            label = {
                Text(text = "Email")
            },
            shape = RoundedCornerShape(12.dp),
            singleLine = true,
            value = state.signUpEmail,
            onValueChange = {
                if (it.length <= Constants.MAX_LENGTH_EMAIL) {
                    viewModel.onEvent(AuthUiEvent.SignUpEmailChanged(it))
                }
            },
            isError = state.emailError != null,
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Email,
                imeAction = ImeAction.Next
            )
        )
        if (state.emailError != null) {
            Text(
                text = state.emailError,
                fontSize = 12.sp,
                color = MaterialTheme.colors.error,
                modifier = Modifier
                    .padding(top = 4.dp)
                    .align(Alignment.Start)
            )
        }

        Spacer(modifier = Modifier.height(16.dp))
        var passwordVisibility by remember { mutableStateOf(false) }
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            colors = TextFieldDefaults.outlinedTextFieldColors(
                cursorColor = Blue,
                disabledLabelColor = LightBlue,
                focusedLabelColor = Blue,
                focusedBorderColor = Blue,
                unfocusedBorderColor = LightBlue,

                ),
            label = {
                Text(text = "Password")
            },
            shape = RoundedCornerShape(12.dp),
            singleLine = true,
            value = state.signUpPassword,
            onValueChange = {
                if (it.length <= Constants.MAX_LENGTH_PASSWORD) {
                    viewModel.onEvent(AuthUiEvent.SignUpPasswordChanged(it))
                }
            },
            isError = state.passwordError != null,
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Next
            ),
            visualTransformation = if (passwordVisibility) VisualTransformation.None else PasswordVisualTransformation(),
            trailingIcon = {
                val image = if (passwordVisibility) {
                    Icons.Filled.Visibility
                } else {
                    Icons.Filled.VisibilityOff
                }

                // Please provide localized description for accessibility services
                val description = if (passwordVisibility) "Hide password" else "Show password"

                IconButton(onClick = { passwordVisibility = !passwordVisibility }) {
                    Icon(imageVector = image, description)
                }
            }
        )
        if (state.passwordError != null) {
            Text(
                text = state.passwordError,
                fontSize = 12.sp,
                color = MaterialTheme.colors.error,
                modifier = Modifier
                    .padding(top = 4.dp)
                    .align(Alignment.Start)
            )
        }

        Spacer(modifier = Modifier.height(16.dp))
        var repeatedPasswordVisibility by remember { mutableStateOf(false) }
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            colors = TextFieldDefaults.outlinedTextFieldColors(
                cursorColor = Blue,
                disabledLabelColor = LightBlue,
                focusedLabelColor = Blue,
                focusedBorderColor = Blue,
                unfocusedBorderColor = LightBlue,

                ),
            label = {
                Text(text = "Password Confirmation")
            },
            shape = RoundedCornerShape(12.dp),
            singleLine = true,
            value = state.signUpRepeatedPassword,
            onValueChange = {
                if (it.length <= Constants.MAX_LENGTH_PASSWORD) {
                    viewModel.onEvent(AuthUiEvent.SignUpRepeatedPasswordChanged(it))
                }
            },
            isError = state.repeatedPasswordError != null,
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Done
            ),
            visualTransformation = if (repeatedPasswordVisibility) VisualTransformation.None else PasswordVisualTransformation(),
            trailingIcon = {
                val image = if (repeatedPasswordVisibility) {
                    Icons.Filled.Visibility
                } else {
                    Icons.Filled.VisibilityOff
                }

                // Please provide localized description for accessibility services
                val description =
                    if (repeatedPasswordVisibility) "Hide password" else "Show password"

                IconButton(onClick = { repeatedPasswordVisibility = !repeatedPasswordVisibility }) {
                    Icon(imageVector = image, description)
                }
            }
        )
        if (state.repeatedPasswordError != null) {
            Text(
                text = state.repeatedPasswordError,
                fontSize = 12.sp,
                color = MaterialTheme.colors.error,
                modifier = Modifier
                    .padding(top = 4.dp)
                    .align(Alignment.Start)
            )
        }

        Spacer(modifier = Modifier.height(16.dp))
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.CenterHorizontally),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Blue
            ),
            shape = RoundedCornerShape(12.dp),
            contentPadding = PaddingValues(16.dp),
            onClick = {
                viewModel.onEvent(AuthUiEvent.SignUp)
            }
        ) {
            Text(
                text = "Sign Up",
                color = Color.White,
                letterSpacing = 0.sp
            )
        }

        Spacer(modifier = Modifier.height(30.dp))
        Row {
            Text(
                text = "Have an account?",
                fontSize = 14.sp
            )
            Spacer(modifier = Modifier.width(4.dp))
            ClickableText(
                text = AnnotatedString("Sign In"),
                style = TextStyle(
                    fontSize = 14.sp,
                    color = Blue
                ),
                onClick = {
                    navigator.popBackStack()
                }
            )
        }
        if (state.isLoading) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White),
                contentAlignment = Alignment.Center
            ) {
                CircularProgressIndicator()
            }
        }
    }
}