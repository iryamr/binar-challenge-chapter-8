package id.binar.chapter8.challenge.data.repository

import id.binar.chapter8.challenge.data.mapper.toDomain
import id.binar.chapter8.challenge.data.source.remote.service.ApiService
import id.binar.chapter8.challenge.domain.model.movie.Movie
import id.binar.chapter8.challenge.domain.repository.MovieRepository
import id.binar.chapter8.challenge.util.Result
import retrofit2.HttpException
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val apiService: ApiService
) : MovieRepository {

    override suspend fun getNowPlayingMovies(): Result<List<Movie>> {
        return try {
            val response = apiService.fetchNowPlayingMovies()
            Result.Success(response.results.map { it.toDomain() })
        } catch (e: HttpException) {
            Result.Error("Please check your connection")
        } catch (e: Exception) {
            Result.Error(e.localizedMessage?.toString() ?: "Unknown error")
        }
    }

    override suspend fun getMovieDetail(movieId: Int): Result<Movie> {
        return try {
            val response = apiService.fetchMovieDetails(movieId)
            Result.Success(response.toDomain())
        } catch (e: HttpException) {
            Result.Error("Please check your connection")
        } catch (e: Exception) {
            Result.Error("Unknown error")
        }
    }
}