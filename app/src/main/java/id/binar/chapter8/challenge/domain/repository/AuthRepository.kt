package id.binar.chapter8.challenge.domain.repository

import id.binar.chapter8.challenge.data.source.local.entity.UserAndProfile
import id.binar.chapter8.challenge.domain.model.auth.Profile
import id.binar.chapter8.challenge.domain.model.auth.User
import id.binar.chapter8.challenge.util.AuthResult
import id.binar.chapter8.challenge.util.Result

interface AuthRepository {

    suspend fun signUp(email: String, username: String, password: String): AuthResult<Unit>

    suspend fun signIn(email: String, password: String): AuthResult<Unit>

    suspend fun signOut(): AuthResult<Unit>

    fun authenticate(): AuthResult<Unit>

    suspend fun getUser(): Result<User>

    suspend fun getProfile(userId: Int): Result<UserAndProfile>

    suspend fun updateUser(user: User)

    suspend fun updateProfile(profile: Profile)
}