package id.binar.chapter8.challenge.presentation.profile

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.launch
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import id.binar.chapter8.challenge.R
import id.binar.chapter8.challenge.domain.model.auth.Profile
import id.binar.chapter8.challenge.domain.model.auth.User
import id.binar.chapter8.challenge.ui.theme.Blue
import id.binar.chapter8.challenge.ui.theme.LightBlue
import id.binar.chapter8.challenge.util.Constants
import id.binar.chapter8.challenge.util.Helper.bitmapToString
import kotlinx.coroutines.launch

@ExperimentalMaterialApi
@Composable
@Destination
fun ProfileScreen(
    userId: Int,
    navigator: DestinationsNavigator,
    viewModel: ProfileViewModel = hiltViewModel()
) {
    val context = LocalContext.current
    val state = viewModel.state
    val bottomSheetModalState = rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)
    val coroutineScope = rememberCoroutineScope()
    val systemUiController = rememberSystemUiController()

    SideEffect {
        systemUiController.setStatusBarColor(
            color = Color.Transparent,
            darkIcons = true
        )
    }

    var isCameraSelected = false
    var imageUri: Uri? = null
    var imageBitmap: Bitmap? = null

    var user: User? = null
    var profile: Profile? = null

    val galleryLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.GetContent()
    ) { uri: Uri? ->
        var uriString = "1_${uri.toString()}"
        imageUri = uri
        imageBitmap = null
    }

    val cameraLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.TakePicturePreview()
    ) { bitmap: Bitmap? ->
        var bitmapString = bitmapToString(bitmap)
        imageUri = null
        imageBitmap = bitmap
    }

    val permissionLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            if (isCameraSelected) {
                cameraLauncher.launch()
            } else {
                galleryLauncher.launch("image/*")
            }
            coroutineScope.launch {
                bottomSheetModalState.hide()
            }
        } else {
            Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show()
        }
    }

    ModalBottomSheetLayout(
        sheetState = bottomSheetModalState,
        sheetShape = RoundedCornerShape(topStart = 12.dp, topEnd = 12.dp),
        sheetContent = {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.SpaceEvenly
                ) {
                    Text(
                        text = "Add Photo",
                        fontSize = 20.sp,
                        fontWeight = FontWeight.Bold,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(15.dp)
                    )
                    Text(
                        text = "Take a Photo",
                        fontSize = 18.sp,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp)
                            .clickable {
                                when (PackageManager.PERMISSION_GRANTED) {
                                    ContextCompat.checkSelfPermission(
                                        context,
                                        Manifest.permission.CAMERA
                                    ) -> {
                                        cameraLauncher.launch()
                                        coroutineScope.launch {
                                            bottomSheetModalState.hide()
                                        }
                                    }
                                    else -> {
                                        isCameraSelected = true
                                        permissionLauncher.launch(Manifest.permission.CAMERA)
                                    }
                                }
                            },
                    )
                    Text(
                        text = "Choose from Gallery",
                        fontSize = 18.sp,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp)
                            .clickable {
                                when (PackageManager.PERMISSION_GRANTED) {
                                    ContextCompat.checkSelfPermission(
                                        context,
                                        Manifest.permission.READ_EXTERNAL_STORAGE
                                    ) -> {
                                        galleryLauncher.launch("image/*")
                                        coroutineScope.launch {
                                            bottomSheetModalState.hide()
                                        }
                                    }
                                    else -> {
                                        isCameraSelected = false
                                        permissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
                                    }
                                }
                            },
                    )
                    Text(
                        text = "Cancel",
                        fontSize = 18.sp,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp)
                            .clickable {
                                coroutineScope.launch {
                                    bottomSheetModalState.hide()
                                }
                            },
                    )
                }
            }
        }
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier
                .fillMaxSize()
                .padding(32.dp)
        ) {
            AsyncImage(
                model = null,
                placeholder = painterResource(R.drawable.ic_photo),
                fallback = painterResource(R.drawable.ic_photo),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(150.dp)
                    .clip(CircleShape)
                    .clickable {
                        coroutineScope.launch {
                            if (!bottomSheetModalState.isVisible) {
                                bottomSheetModalState.show()
                            } else {
                                bottomSheetModalState.hide()
                            }
                        }
                    },
            )

            Spacer(modifier = Modifier.height(24.dp))
            OutlinedTextField(
                modifier = Modifier.fillMaxWidth(),
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    cursorColor = Blue,
                    disabledLabelColor = LightBlue,
                    focusedLabelColor = Blue,
                    focusedBorderColor = Blue,
                    unfocusedBorderColor = LightBlue,
                ),
                label = {
                    Text(text = "Username")
                },
                shape = RoundedCornerShape(12.dp),
                singleLine = true,
                value = state.username,
                onValueChange = {
                    if (it.length <= Constants.MAX_LENGTH_USERNAME) {
                        viewModel.onEvent(ProfileUiEvent.UsernameChanged(it))
                    }
                },
                isError = state.usernameError != null,
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
            )
            if (state.usernameError != null) {
                Text(
                    text = state.usernameError,
                    fontSize = 12.sp,
                    color = MaterialTheme.colors.error,
                    modifier = Modifier
                        .padding(top = 4.dp)
                        .align(Alignment.Start)
                )
            }

            Spacer(modifier = Modifier.height(8.dp))
            OutlinedTextField(
                modifier = Modifier.fillMaxWidth(),
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    cursorColor = Blue,
                    disabledLabelColor = LightBlue,
                    focusedLabelColor = Blue,
                    focusedBorderColor = Blue,
                    unfocusedBorderColor = LightBlue,
                ),
                label = {
                    Text(text = "Name")
                },
                shape = RoundedCornerShape(12.dp),
                singleLine = true,
                value = state.name,
                onValueChange = {
                    if (it.length <= Constants.MAX_LENGTH_USERNAME) {
                        viewModel.onEvent(ProfileUiEvent.NameChanged(it))
                    }
                },
                isError = state.nameError != null,
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
            )
            if (state.nameError != null) {
                Text(
                    text = state.nameError,
                    fontSize = 12.sp,
                    color = MaterialTheme.colors.error,
                    modifier = Modifier
                        .padding(top = 4.dp)
                        .align(Alignment.Start)
                )
            }

            Spacer(modifier = Modifier.height(8.dp))
            OutlinedTextField(
                modifier = Modifier.fillMaxWidth(),
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    cursorColor = Blue,
                    disabledLabelColor = LightBlue,
                    focusedLabelColor = Blue,
                    focusedBorderColor = Blue,
                    unfocusedBorderColor = LightBlue,
                ),
                label = {
                    Text(text = "Birthday")
                },
                shape = RoundedCornerShape(12.dp),
                singleLine = true,
                value = state.birthday,
                onValueChange = {
                    if (it.length <= Constants.MAX_LENGTH_USERNAME) {
                        viewModel.onEvent(ProfileUiEvent.BirthdayChanged(it))
                    }
                },
                isError = state.birthdayError != null,
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
            )
            if (state.birthdayError != null) {
                Text(
                    text = state.birthdayError,
                    fontSize = 12.sp,
                    color = MaterialTheme.colors.error,
                    modifier = Modifier
                        .padding(top = 4.dp)
                        .align(Alignment.Start)
                )
            }

            Spacer(modifier = Modifier.height(8.dp))
            OutlinedTextField(
                modifier = Modifier.fillMaxWidth(),
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    cursorColor = Blue,
                    disabledLabelColor = LightBlue,
                    focusedLabelColor = Blue,
                    focusedBorderColor = Blue,
                    unfocusedBorderColor = LightBlue,
                ),
                label = {
                    Text(text = "Address")
                },
                shape = RoundedCornerShape(12.dp),
                singleLine = true,
                value = state.address,
                onValueChange = {
                    if (it.length <= Constants.MAX_LENGTH_USERNAME) {
                        viewModel.onEvent(ProfileUiEvent.AddressChanged(it))
                    }
                },
                isError = state.addressError != null,
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
            )
            if (state.addressError != null) {
                Text(
                    text = state.addressError,
                    fontSize = 12.sp,
                    color = MaterialTheme.colors.error,
                    modifier = Modifier
                        .padding(top = 4.dp)
                        .align(Alignment.Start)
                )
            }

            Spacer(modifier = Modifier.height(16.dp))
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.CenterHorizontally),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Blue
                ),
                shape = RoundedCornerShape(12.dp),
                contentPadding = PaddingValues(16.dp),
                onClick = {
                    viewModel.onEvent(ProfileUiEvent.Update)
//                        navigator.popBackStack()
                }
            ) {
                Text(
                    text = "Update Profile",
                    color = Color.White,
                    letterSpacing = 0.sp
                )
            }
        }
    }
}

