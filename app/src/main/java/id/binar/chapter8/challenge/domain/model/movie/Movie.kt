package id.binar.chapter8.challenge.domain.model.movie

data class Movie(
    val id: Int,
    val title: String,
    val genres: List<Genre>?,
    val posterPath: String,
    val backdropPath: String?,
    val releaseDate: String,
    val overview: String,
    val voteAverage: Double,
)