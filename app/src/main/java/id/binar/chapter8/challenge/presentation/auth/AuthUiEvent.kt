package id.binar.chapter8.challenge.presentation.auth

sealed class AuthUiEvent {
    data class SignUpEmailChanged(val value: String) : AuthUiEvent()
    data class SignUpUsernameChanged(val value: String) : AuthUiEvent()
    data class SignUpPasswordChanged(val value: String) : AuthUiEvent()
    data class SignUpRepeatedPasswordChanged(val value: String) : AuthUiEvent()
    object SignUp : AuthUiEvent()

    data class SignInEmailChanged(val value: String) : AuthUiEvent()
    data class SignInUsernameChanged(val value: String) : AuthUiEvent()
    data class SignInPasswordChanged(val value: String) : AuthUiEvent()
    object SignIn : AuthUiEvent()

    object SignOut : AuthUiEvent()
}
