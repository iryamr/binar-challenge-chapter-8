package id.binar.chapter8.challenge.domain.model.movie

data class Genre(
    val id: Int,
    val name: String,
)
