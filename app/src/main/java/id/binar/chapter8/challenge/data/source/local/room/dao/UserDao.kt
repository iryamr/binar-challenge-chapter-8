package id.binar.chapter8.challenge.data.source.local.room.dao

import androidx.room.*
import id.binar.chapter8.challenge.data.source.local.entity.ProfileEntity
import id.binar.chapter8.challenge.data.source.local.entity.UserAndProfile
import id.binar.chapter8.challenge.data.source.local.entity.UserEntity

@Dao
interface UserDao {

    @Insert
    suspend fun signUp(user: UserEntity)

    @Query("SELECT * FROM users WHERE email = :email AND password = :password")
    suspend fun signIn(email: String, password: String): UserEntity

    @Query("SELECT EXISTS(SELECT * FROM users WHERE email = :email)")
    suspend fun checkEmail(email: String): Boolean

    @Query("SELECT EXISTS(SELECT * FROM users WHERE email = :email AND password = :password LIMIT 1)")
    suspend fun checkCredentials(email: String, password: String): Boolean

    @Query("SELECT * FROM users WHERE email = :email LIMIT 1")
    suspend fun getUser(email: String): UserEntity

    @Update
    suspend fun updateUser(user: UserEntity): Int

    // Query for profile

    @Insert
    suspend fun createProfile(profile: ProfileEntity): Long

    @Update
    suspend fun updateProfile(profile: ProfileEntity): Int

    @Transaction
    @Query("SELECT * FROM users WHERE id = :userId LIMIT 1")
    suspend fun getProfile(userId: Int): UserAndProfile
}