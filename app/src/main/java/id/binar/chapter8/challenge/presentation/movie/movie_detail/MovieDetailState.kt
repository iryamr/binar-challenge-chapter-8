package id.binar.chapter8.challenge.presentation.movie.movie_detail

import id.binar.chapter8.challenge.domain.model.movie.Movie

data class MovieDetailState(
    val isLoading: Boolean = false,
    val movie: Movie? = null,
)
