package id.binar.chapter8.challenge.data.source.remote.service

import id.binar.chapter8.challenge.data.source.remote.response.BaseResponse
import id.binar.chapter8.challenge.data.source.remote.response.MovieDto
import id.binar.chapter8.challenge.util.Endpoints
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET(Endpoints.NOW_PLAYING_MOVIES)
    suspend fun fetchNowPlayingMovies(): BaseResponse<MovieDto>

    @GET(Endpoints.MOVIE_DETAIL)
    suspend fun fetchMovieDetails(
        @Path("movie_id") movieId: Int
    ): MovieDto
}