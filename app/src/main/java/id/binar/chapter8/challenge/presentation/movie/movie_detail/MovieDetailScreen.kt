package id.binar.chapter8.challenge.presentation.movie.movie_detail

import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBackIos
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import id.binar.chapter8.challenge.R
import id.binar.chapter8.challenge.domain.model.movie.Genre
import id.binar.chapter8.challenge.ui.component.CircularProgressBar
import id.binar.chapter8.challenge.ui.theme.Blue
import id.binar.chapter8.challenge.util.Constants
import id.binar.chapter8.challenge.util.Helper.toYear

@Composable
@Destination
fun MovieDetailScreen(
    movieId: Int,
    navigator: DestinationsNavigator,
    viewModel: MovieDetailViewModel = hiltViewModel()
) {
    val state = viewModel.state
    val scrollState = rememberScrollState()
    val systemUiController = rememberSystemUiController()

    SideEffect {
        systemUiController.setStatusBarColor(
            color = Color.Transparent,
            darkIcons = false
        )
    }

    state.movie?.let { movie ->
        Box(
            modifier = Modifier
                .fillMaxSize()
                .scrollable(
                    state = scrollState,
                    orientation = Orientation.Vertical
                )
        ) {
            AsyncImage(
                model = Constants.IMG_URL + movie.backdropPath,
                fallback = painterResource(R.drawable.ic_image),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(300.dp)
                    .clip(RoundedCornerShape(bottomStart = 12.dp, bottomEnd = 12.dp)),
            )
            IconButton(
                modifier = Modifier.systemBarsPadding(),
                onClick = { navigator.popBackStack() },
            ) {
                Icon(
                    imageVector = Icons.Rounded.ArrowBackIos,
                    contentDescription = null,
                    tint = Color.White
                )
            }
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp)
            ) {
                Row(
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.Top,
                    modifier = Modifier
                        .padding(top = 200.dp)
                ) {
                    AsyncImage(
                        model = Constants.IMG_URL + movie.posterPath,
                        fallback = painterResource(R.drawable.ic_image),
                        contentDescription = null,
                        contentScale = ContentScale.Crop,
                        modifier = Modifier
                            .size(width = 130.dp, height = 200.dp)
                            .clip(RoundedCornerShape(12.dp)),
                    )
                    Column(
                        modifier = Modifier.padding(8.dp),
                    ) {
                        Column(
                            modifier = Modifier.height(70.dp)
                        ) {
                            CircularProgressBar(
                                percentage = 0.5f,
                                modifier = Modifier.offset(x = 4.dp)
                            )

                            val list = ArrayList<Genre>()
                            movie.genres?.forEach { list.add(it) }
                            val genres = list.joinToString { it.name }

                            Spacer(modifier = Modifier.height(8.dp))
                            Text(
                                text = genres,
                                fontSize = 14.sp,
                                color = Color.White
                            )
                        }

                        Spacer(modifier = Modifier.height(12.dp))
                        Text(
                            buildAnnotatedString {
                                withStyle(
                                    style = SpanStyle(
                                        color = Color.Black,
                                        fontSize = 20.sp,
                                        fontWeight = FontWeight.Bold,
                                    )
                                ) {
                                    append(movie.title)
                                }
                                withStyle(
                                    style = SpanStyle(
                                        color = Color.DarkGray,
                                        fontSize = 20.sp,
                                    )
                                ) {
                                    append(" ${movie.releaseDate.toYear()}")
                                }
                            }
                        )
                    }
                }

                Spacer(modifier = Modifier.height(12.dp))
                Text(text = "Overview", fontSize = 20.sp, fontWeight = FontWeight.Bold)

                Divider(
                    modifier = Modifier
                        .size(width = 73.dp, height = 4.dp)
                        .clip(RoundedCornerShape(12.dp)),
                    color = Blue
                )

                Spacer(modifier = Modifier.height(8.dp))
                Text(text = movie.overview)
            }
        }
    }

    if (state.isLoading) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.White),
            contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator()
        }
    }
}