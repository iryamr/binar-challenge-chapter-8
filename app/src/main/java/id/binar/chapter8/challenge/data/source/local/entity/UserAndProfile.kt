package id.binar.chapter8.challenge.data.source.local.entity

import androidx.room.Embedded
import androidx.room.Relation

data class UserAndProfile(
    @Embedded
    val user: UserEntity,

    @Relation(
        parentColumn = "id",
        entityColumn = "userId"
    )
    val profile: ProfileEntity
)
