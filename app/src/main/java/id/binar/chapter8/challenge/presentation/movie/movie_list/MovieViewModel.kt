package id.binar.chapter8.challenge.presentation.movie.movie_list

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter8.challenge.domain.model.movie.Movie
import id.binar.chapter8.challenge.domain.repository.MovieRepository
import id.binar.chapter8.challenge.util.Result
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(
    private val repository: MovieRepository
) : ViewModel() {

    var state by mutableStateOf(MovieListState())

    private val resultChannel = Channel<Result<List<Movie>>>()

    init {
        getNowPlayingMovies()
    }

    private fun getNowPlayingMovies() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = repository.getNowPlayingMovies()
            result.data?.let {
                state = state.copy(movies = it)
            }
            state = state.copy(isLoading = false)
            resultChannel.send(result)
        }
    }
}