package id.binar.chapter8.challenge.data.source.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import id.binar.chapter8.challenge.data.source.local.entity.ProfileEntity
import id.binar.chapter8.challenge.data.source.local.entity.UserEntity
import id.binar.chapter8.challenge.data.source.local.room.dao.UserDao

@Database(
    entities = [UserEntity::class, ProfileEntity::class],
    version = 1,
    exportSchema = false
)
abstract class MovieDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
}