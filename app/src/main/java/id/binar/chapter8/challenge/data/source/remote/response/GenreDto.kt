package id.binar.chapter8.challenge.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class GenreDto(

    @SerializedName("id")
    val id: Int,

    @SerializedName("name")
    val name: String,
)
