package id.binar.chapter8.challenge.domain.model.auth

data class User(
    var id: Int,
    var username: String,
    var email: String,
    var password: String
)
