package id.binar.chapter8.challenge.presentation.auth

import android.util.Patterns
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter8.challenge.domain.model.auth.User
import id.binar.chapter8.challenge.domain.repository.AuthRepository
import id.binar.chapter8.challenge.util.AuthResult
import id.binar.chapter8.challenge.util.Constants
import id.binar.chapter8.challenge.util.Result
import id.binar.chapter8.challenge.util.ValidationResult
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val repository: AuthRepository
) : ViewModel() {

    var state by mutableStateOf(AuthState())

    private val authChannel = Channel<AuthResult<Unit>>()
    val authResult = authChannel.receiveAsFlow()

    private val userChannel = Channel<Result<User>>()
    val userResult = userChannel.receiveAsFlow()

    init {
        authenticate()
        getUser()
    }

    fun onEvent(event: AuthUiEvent) {
        when (event) {
            is AuthUiEvent.SignUpEmailChanged -> {
                state = state.copy(signUpEmail = event.value)
            }
            is AuthUiEvent.SignUpUsernameChanged -> {
                state = state.copy(signUpUsername = event.value)
            }
            is AuthUiEvent.SignUpPasswordChanged -> {
                state = state.copy(signUpPassword = event.value)
            }
            is AuthUiEvent.SignUpRepeatedPasswordChanged -> {
                state = state.copy(signUpRepeatedPassword = event.value)
            }
            is AuthUiEvent.SignUp -> {
                signUp()
            }
            is AuthUiEvent.SignInEmailChanged -> {
                state = state.copy(signInEmail = event.value)
            }
            is AuthUiEvent.SignInUsernameChanged -> {
                state = state.copy(signInUsername = event.value)
            }
            is AuthUiEvent.SignInPasswordChanged -> {
                state = state.copy(signInPassword = event.value)
            }
            is AuthUiEvent.SignIn -> {
                signIn()
            }
            is AuthUiEvent.SignOut -> {
                signOut()
            }
        }
    }

    private fun signUp() {
        val emailResult = validateEmail(state.signUpEmail)
        val usernameResult = validateUsername(state.signUpUsername)
        val passwordResult = validatePassword(state.signUpPassword)
        val repeatedPasswordResult = validateRepeatedPassword(
            state.signUpPassword,
            state.signUpRepeatedPassword
        )

        val hasError = listOf(
            emailResult,
            usernameResult,
            passwordResult,
            repeatedPasswordResult
        ).any { !it.successful }

        if (hasError) {
            state = state.copy(
                emailError = emailResult.errorMessage,
                usernameError = usernameResult.errorMessage,
                passwordError = passwordResult.errorMessage,
                repeatedPasswordError = repeatedPasswordResult.errorMessage,
            )
            return
        }

        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = repository.signUp(
                email = state.signUpEmail,
                username = state.signUpUsername,
                password = state.signUpPassword
            )
            authChannel.send(result)
            state = state.copy(isLoading = false)
        }
    }

    private fun signIn() {
        val emailResult = validateEmail(state.signInEmail)
        val passwordResult = validatePassword(state.signInPassword)

        val hasError = listOf(
            emailResult,
            passwordResult,
        ).any { !it.successful }

        if (hasError) {
            state = state.copy(
                emailError = emailResult.errorMessage,
                passwordError = passwordResult.errorMessage,
            )
            return
        }

        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = repository.signIn(
                email = state.signInEmail,
                password = state.signInPassword
            )
            authChannel.send(result)
            state = state.copy(isLoading = false)
        }
    }

    private fun signOut() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = repository.signOut()
            authChannel.send(result)
            state = state.copy(isLoading = false)
        }
    }

    private fun authenticate() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = repository.authenticate()
            authChannel.send(result)
            state = state.copy(isLoading = false)
        }
    }

    private fun getUser() {
        viewModelScope.launch {
            state = state.copy(isLoading = true)
            val result = repository.getUser()
            result.data?.let {
                state = state.copy(user = it)
            }
            userChannel.send(result)
            state = state.copy(isLoading = false)
        }
    }

    private fun validateEmail(email: String): ValidationResult {
        if (email.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "The email cannot be blank"
            )
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return ValidationResult(
                successful = false,
                errorMessage = "The email is not valid"
            )
        }
        return ValidationResult(
            successful = true,
        )
    }

    private fun validateUsername(username: String): ValidationResult {
        if (username.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "The username can't be blank"
            )
        }
        return ValidationResult(
            successful = true,
        )
    }

    private fun validatePassword(password: String): ValidationResult {
        if (password.length < Constants.MIN_LENGTH_PASSWORD) {
            return ValidationResult(
                successful = false,
                errorMessage = "The password need at least 6 characters"
            )
        }
        return ValidationResult(
            successful = true,
        )
    }

    private fun validateRepeatedPassword(
        password: String,
        repeatedPassword: String
    ): ValidationResult {
        if (password != repeatedPassword) {
            return ValidationResult(
                successful = false,
                errorMessage = "The password confirmation doesn't match"
            )
        }
        return ValidationResult(
            successful = true,
        )
    }
}