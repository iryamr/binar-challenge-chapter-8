package id.binar.chapter8.challenge.presentation.profile

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter8.challenge.data.mapper.toProfile
import id.binar.chapter8.challenge.data.mapper.toUser
import id.binar.chapter8.challenge.data.source.local.entity.UserAndProfile
import id.binar.chapter8.challenge.domain.repository.AuthRepository
import id.binar.chapter8.challenge.util.Result
import id.binar.chapter8.challenge.util.ValidationResult
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val repository: AuthRepository
) : ViewModel() {

    var state by mutableStateOf(ProfileState())

    private val resultChannel = Channel<Result<UserAndProfile>>()

    init {
        getProfile()
    }

    fun onEvent(event: ProfileUiEvent) {
        when (event) {
            is ProfileUiEvent.UsernameChanged -> {
                state = state.copy(username = event.value)
            }
            is ProfileUiEvent.NameChanged -> {
                state = state.copy(name = event.value)
            }
            is ProfileUiEvent.BirthdayChanged -> {
                state = state.copy(birthday = event.value)
            }
            is ProfileUiEvent.AddressChanged -> {
                state = state.copy(address = event.value)
            }
            is ProfileUiEvent.Update -> {
                updateProfile()
            }
        }
    }

    private fun getProfile() {
        viewModelScope.launch {
            val userId = savedStateHandle.get<Int>("userId") ?: return@launch
            state = state.copy(isLoading = true)
            val result = repository.getProfile(userId)
            result.data?.let {
                state = state.copy(
                    isLoading = false,
                    user = it.user.toUser(),
                    username = it.user.toUser().username,
                    profile = it.profile.toProfile(),
                    photo = it.profile.toProfile().photo ?: "",
                    name = it.profile.toProfile().name ?: "",
                    birthday = it.profile.toProfile().birthday ?: "",
                    address = it.profile.toProfile().address ?: "",
                )
            }
            resultChannel.send(result)
        }
    }

    private fun updateProfile() {
        val usernameResult = validateUsername(state.username)
        val nameResult = validateName(state.name)
        val birthdayResult = validateBirthday(state.birthday)
        val addressResult = validateAddress(state.address)

        val hasError = listOf(
            usernameResult,
            nameResult,
            birthdayResult,
            addressResult
        ).any { !it.successful }

        if (hasError) {
            state = state.copy(
                usernameError = usernameResult.errorMessage,
                nameError = nameResult.errorMessage,
                birthdayError = birthdayResult.errorMessage,
                addressError = addressResult.errorMessage
            )
            return
        }

        viewModelScope.launch {
            state = state.copy(isLoading = true)
            state.user?.let {
                it.username = state.username
                repository.updateUser(it)
            }
            state.profile?.let {
                it.name = state.name
                it.birthday = state.birthday
                it.address = state.address
                repository.updateProfile(it)
            }
            state = state.copy(isLoading = false)
        }
    }

    private fun validateUsername(username: String): ValidationResult {
//        username?.let {
        if (username.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "The username cannot be blank"
            )
        }
//        }
        return ValidationResult(
            successful = true,
        )
    }

    private fun validateName(name: String): ValidationResult {
//        name?.let {
        if (name.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "The name cannot be blank"
            )
        }
//        }
        return ValidationResult(
            successful = true,
        )
    }

    private fun validateBirthday(birthday: String): ValidationResult {
//        birthday?.let {
        if (birthday.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "The birthday cannot be blank"
            )
        }
//        }
        return ValidationResult(
            successful = true,
        )
    }

    private fun validateAddress(address: String): ValidationResult {
//        address?.let {
        if (address.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "The address cannot be blank"
            )
        }
//        }
        return ValidationResult(
            successful = true,
        )
    }
}