package id.binar.chapter8.challenge.util

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class UserPreferences @Inject constructor(
    private val dataStore: DataStore<Preferences>
) {

    fun getAuthenticatedEmail(): String {
        return runBlocking {
            dataStore.data.first()
        }[EMAIL_KEY] ?: ""
    }

    suspend fun signIn(email: String) {
        dataStore.edit { preferences ->
            preferences[EMAIL_KEY] = email
        }
    }

    suspend fun signOut() {
        dataStore.edit { preferences ->
            preferences[EMAIL_KEY] = ""
        }
    }

    companion object {
        private val EMAIL_KEY = stringPreferencesKey("email")
    }
}