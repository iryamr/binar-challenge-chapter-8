package id.binar.chapter8.challenge.presentation.profile

sealed class ProfileUiEvent {
    data class UsernameChanged(val value: String) : ProfileUiEvent()
    data class NameChanged(val value: String) : ProfileUiEvent()
    data class BirthdayChanged(val value: String) : ProfileUiEvent()
    data class AddressChanged(val value: String) : ProfileUiEvent()
    object Update : ProfileUiEvent()
}
