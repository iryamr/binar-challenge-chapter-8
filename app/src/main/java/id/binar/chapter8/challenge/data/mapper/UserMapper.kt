package id.binar.chapter8.challenge.data.mapper

import id.binar.chapter8.challenge.data.source.local.entity.ProfileEntity
import id.binar.chapter8.challenge.data.source.local.entity.UserEntity
import id.binar.chapter8.challenge.domain.model.auth.Profile
import id.binar.chapter8.challenge.domain.model.auth.User

fun UserEntity.toUser(): User {
    return User(
        id = id,
        username = username,
        email = email,
        password = password,
    )
}

fun User.toUserEntity(): UserEntity {
    return UserEntity(
        id = id,
        username = username,
        email = email,
        password = password,
    )
}

fun ProfileEntity.toProfile(): Profile {
    return Profile(
        id = id,
        userId = userId,
        name = name,
        address = address,
        birthday = birthday,
        photo = photo,
    )
}

fun Profile.toProfileEntity(): ProfileEntity {
    return ProfileEntity(
        id = id,
        userId = userId,
        name = name,
        address = address,
        birthday = birthday,
        photo = photo,
    )
}
