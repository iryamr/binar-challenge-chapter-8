package id.binar.chapter8.challenge.data.repository

import id.binar.chapter8.challenge.data.mapper.toProfileEntity
import id.binar.chapter8.challenge.data.mapper.toUser
import id.binar.chapter8.challenge.data.mapper.toUserEntity
import id.binar.chapter8.challenge.data.source.local.entity.ProfileEntity
import id.binar.chapter8.challenge.data.source.local.entity.UserAndProfile
import id.binar.chapter8.challenge.data.source.local.entity.UserEntity
import id.binar.chapter8.challenge.data.source.local.room.dao.UserDao
import id.binar.chapter8.challenge.domain.model.auth.Profile
import id.binar.chapter8.challenge.domain.model.auth.User
import id.binar.chapter8.challenge.domain.repository.AuthRepository
import id.binar.chapter8.challenge.util.AuthResult
import id.binar.chapter8.challenge.util.Result
import id.binar.chapter8.challenge.util.UserPreferences
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val dao: UserDao,
    private val prefs: UserPreferences
) : AuthRepository {

    override suspend fun signUp(
        email: String,
        username: String,
        password: String
    ): AuthResult<Unit> {
        return try {
            val user = UserEntity(
                email = email,
                username = username,
                password = password
            )
            dao.signUp(user)

            val result = dao.signIn(email, password)
            prefs.signIn(result.email)

            val profile = ProfileEntity(
                userId = result.id
            )
            dao.createProfile(profile)

            AuthResult.Authorized()
        } catch (e: Exception) {
            AuthResult.UnknownError()
        }
    }

    override suspend fun signIn(
        email: String,
        password: String
    ): AuthResult<Unit> {
        return try {
            val result = dao.signIn(email, password)
            prefs.signIn(result.email)
            AuthResult.Authorized()
        } catch (e: NullPointerException) {
            AuthResult.Unauthorized()
        } catch (e: Exception) {
            AuthResult.UnknownError()
        }
    }

    override suspend fun signOut(): AuthResult<Unit> {
        prefs.signOut()
        return AuthResult.Unauthorized()
    }

    override fun authenticate(): AuthResult<Unit> {
        return try {
            val email = prefs.getAuthenticatedEmail()
            if (email.isBlank()) {
                return AuthResult.Unauthorized()
            }
            AuthResult.Authorized()
        } catch (e: NullPointerException) {
            AuthResult.Unauthorized()
        } catch (e: Exception) {
            AuthResult.UnknownError()
        }
    }

    override suspend fun getUser(): Result<User> {
        return try {
            val email = prefs.getAuthenticatedEmail()
            return if (email.isNotBlank()) {
                val user = dao.getUser(email)
                Result.Success(user.toUser())
            } else {
                Result.Error("User not found")
            }
        } catch (e: Exception) {
            Result.Error(e.localizedMessage?.toString() ?: "Unknown Error")
        }
    }

    override suspend fun getProfile(userId: Int): Result<UserAndProfile> {
        return try {
            val user = dao.getProfile(userId)
            Result.Success(user)
        } catch (e: Exception) {
            Result.Error(e.localizedMessage?.toString() ?: "Unknown Error")
        }
    }

    override suspend fun updateUser(user: User) {
        dao.updateUser(user.toUserEntity())
    }

    override suspend fun updateProfile(profile: Profile) {
        dao.updateProfile(profile.toProfileEntity())
    }
}