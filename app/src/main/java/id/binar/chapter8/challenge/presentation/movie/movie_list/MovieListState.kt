package id.binar.chapter8.challenge.presentation.movie.movie_list

import id.binar.chapter8.challenge.domain.model.movie.Movie

data class MovieListState(
    val isLoading: Boolean = false,
    val movies: List<Movie> = emptyList(),
)
