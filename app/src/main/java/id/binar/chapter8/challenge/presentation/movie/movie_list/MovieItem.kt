package id.binar.chapter8.challenge.presentation.movie.movie_list

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import id.binar.chapter8.challenge.R
import id.binar.chapter8.challenge.domain.model.movie.Movie
import id.binar.chapter8.challenge.ui.component.CircularProgressBar
import id.binar.chapter8.challenge.util.Constants
import id.binar.chapter8.challenge.util.Helper.toDate

@Composable
fun MovieItem(
    data: Movie,
    onClick: (Movie) -> Unit
) {
    Card(
        modifier = Modifier
            .fillMaxSize()
            .clip(RoundedCornerShape(12.dp)),
        elevation = 0.dp,
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .clickable {
                    onClick(data)
                },
        ) {
            Box(
                contentAlignment = Alignment.BottomStart,
            ) {
                AsyncImage(
                    model = Constants.IMG_URL + data.posterPath,
                    contentDescription = data.title,
                    contentScale = ContentScale.Crop,
                    fallback = painterResource(R.drawable.ic_image),
                    modifier = Modifier
                        .fillMaxSize()
                        .clip(RoundedCornerShape(10.dp)),
                )
                CircularProgressBar(
                    percentage = data.voteAverage.toFloat() / 10,
                    modifier = Modifier
                        .size(40.dp * 2f)
                        .offset(
                            x = (-10).dp,
                            y = 40.dp
                        )
                )
            }
            Spacer(modifier = Modifier.height(14.dp))
            Column(
                modifier = Modifier.padding(10.dp)
            ) {
                Text(
                    text = data.title,
                    fontWeight = FontWeight.Bold,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
                Text(
                    text = data.releaseDate.toDate()!!,
                    fontSize = 13.sp,
                    color = Color.DarkGray,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis
                )
            }
        }
    }
}