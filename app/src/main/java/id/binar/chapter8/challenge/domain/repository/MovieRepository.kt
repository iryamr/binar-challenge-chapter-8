package id.binar.chapter8.challenge.domain.repository

import id.binar.chapter8.challenge.domain.model.movie.Movie
import id.binar.chapter8.challenge.util.Result

interface MovieRepository {

    suspend fun getNowPlayingMovies(): Result<List<Movie>>

    suspend fun getMovieDetail(movieId: Int): Result<Movie>
}