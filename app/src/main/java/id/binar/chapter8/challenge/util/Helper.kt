package id.binar.chapter8.challenge.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

object Helper {

    fun bitmapToString(bitmap: Bitmap?): String {
        val baos = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.PNG, 100, baos)
        val ba = baos.toByteArray()
        return Base64.encodeToString(ba, Base64.DEFAULT)
    }

    fun String.toBitmap(): Bitmap? {
        return try {
            val decodeByte = Base64.decode(this, Base64.DEFAULT)
            BitmapFactory.decodeByteArray(decodeByte, 0, decodeByte.size)
        } catch (e: Exception) {
            e.message
            null
        }
    }

    fun String.toYear(): String? {
        val inputPattern = "yyyy-MM-dd"
        val outputPattern = "yyyy"

        val inputFormat = SimpleDateFormat(inputPattern, Locale.getDefault())
        val outputFormat = SimpleDateFormat(outputPattern, Locale.getDefault())

        val inputDate = inputFormat.parse(this)

        return if (this != "") {
            inputDate?.let {
                "(${outputFormat.format(it)})"
            }
        } else {
            "-"
        }
    }

    fun String.toDate(): String? {
        val inputPattern = "yyyy-MM-dd"
        val outputPattern = "MMM d, yyyy"

        val inputFormat = SimpleDateFormat(inputPattern, Locale.getDefault())
        val outputFormat = SimpleDateFormat(outputPattern, Locale.getDefault())

        val inputDate = inputFormat.parse(this)

        return if (this.isNotBlank()) {
            inputDate?.let {
                outputFormat.format(it)
            }
        } else {
            "-"
        }
    }
}