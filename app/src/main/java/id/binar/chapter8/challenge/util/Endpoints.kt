package id.binar.chapter8.challenge.util

object Endpoints {

    const val NOW_PLAYING_MOVIES = "movie/now_playing"
    const val POPULAR_MOVIES = "movie/popular"
    const val MOVIE_DETAIL = "movie/{movie_id}"
    const val MOVIE_CASTS = "movie/{movie_id}/credits"
}