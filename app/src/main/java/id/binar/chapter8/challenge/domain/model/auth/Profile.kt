package id.binar.chapter8.challenge.domain.model.auth

data class Profile(
    var id: Int,
    var userId: Int,
    var name: String?,
    var address: String?,
    var birthday: String?,
    var photo: String?,
)
