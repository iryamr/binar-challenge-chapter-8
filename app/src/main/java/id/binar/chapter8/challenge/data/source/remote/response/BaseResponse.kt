package id.binar.chapter8.challenge.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class BaseResponse<out T>(

    @SerializedName("results")
    val results: List<T>
)
