package id.binar.chapter8.challenge.presentation.profile

import id.binar.chapter8.challenge.domain.model.auth.Profile
import id.binar.chapter8.challenge.domain.model.auth.User

data class ProfileState(
    val isLoading: Boolean = false,

    val user: User? = null,
    val profile: Profile? = null,

    val photo: String = "",
    val username: String = "",
    val name: String = "",
    val birthday: String = "",
    val address: String = "",

    val usernameError: String? = null,
    val nameError: String? = null,
    val birthdayError: String? = null,
    val addressError: String? = null,
)
