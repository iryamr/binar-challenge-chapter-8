package id.binar.chapter8.challenge.util

data class ValidationResult(
    val successful: Boolean,
    val errorMessage: String? = null
)