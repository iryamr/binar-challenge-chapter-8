package id.binar.chapter8.challenge.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.binar.chapter8.challenge.data.repository.AuthRepositoryImpl
import id.binar.chapter8.challenge.data.repository.MovieRepositoryImpl
import id.binar.chapter8.challenge.domain.repository.AuthRepository
import id.binar.chapter8.challenge.domain.repository.MovieRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindAuthRepository(
        authRepositoryImpl: AuthRepositoryImpl
    ): AuthRepository

    @Binds
    @Singleton
    abstract fun bindMovieRepository(
        movieRepositoryImpl: MovieRepositoryImpl
    ): MovieRepository
}