package id.binar.chapter8.challenge.util

import id.binar.chapter8.challenge.BuildConfig

object Constants {

    const val MAX_LENGTH_USERNAME = 15
    const val MAX_LENGTH_EMAIL = 50
    const val MAX_LENGTH_PASSWORD = 20

    const val MIN_LENGTH_USERNAME = 5
    const val MIN_LENGTH_EMAIL = 5
    const val MIN_LENGTH_PASSWORD = 6

    const val BASE_URL = BuildConfig.BASE_URL
    const val IMG_URL = BuildConfig.IMG_URL
    const val API_KEY = BuildConfig.API_KEY
}