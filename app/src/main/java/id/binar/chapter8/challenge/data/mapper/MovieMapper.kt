package id.binar.chapter8.challenge.data.mapper

import id.binar.chapter8.challenge.data.source.remote.response.GenreDto
import id.binar.chapter8.challenge.data.source.remote.response.MovieDto
import id.binar.chapter8.challenge.domain.model.movie.Genre
import id.binar.chapter8.challenge.domain.model.movie.Movie

fun MovieDto.toDomain(): Movie {
    val genres = genres?.map { it.toDomain() }
    return Movie(
        id = id,
        title = title,
        genres = genres,
        posterPath = posterPath,
        backdropPath = backdropPath,
        releaseDate = releaseDate,
        overview = overview,
        voteAverage = voteAverage,
    )
}

fun GenreDto.toDomain(): Genre {
    return Genre(
        id = id,
        name = name
    )
}